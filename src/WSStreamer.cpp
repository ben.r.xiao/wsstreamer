#include <string>
#include <vector>

#include "boost/shared_ptr.hpp"
#include "boost/asio.hpp"
#include "boost/thread.hpp"
#include "websocketpp.hpp"
#include "websocket_connection_handler.hpp"

#include "WSStreamer.h"
#include "WSStreamerCtx.h"

using namespace std;

WSStreamer::WSStreamer(string host, string port) {
	ctx = new WSStreamerCtx(host, port);
	string full_host = host + ":" + port;

	// setup server settings
	ctx->server->add_host(full_host);
	ctx->server->add_host("localhost:" + port);

	// start the server
	ctx->server->start_accept();
}

WSStreamer::~WSStreamer() {
	stop();
	delete ctx;
}

void WSStreamer::operator()() {
	ctx->io_service.run();
}

void WSStreamer::run() {
	if (!ctx->iosrv_thread) {
		ctx->iosrv_thread = shared_ptr<boost::thread>(new boost::thread(boost::ref(*this)));
	}
}

void WSStreamer::runAndBlock() {
	ctx->io_service.run();
}

void WSStreamer::stop() {
	ctx->io_service.stop();
	ctx->iosrv_thread->join();
	ctx->iosrv_thread.reset();
}

void WSStreamer::addResource(string rsrc) {
	ctx->streamer->addResource(rsrc);
}

void WSStreamer::removeResource(string rsrc) {
	ctx->streamer->removeResource(rsrc);
}

void WSStreamer::sendAll(string resource, const vector<unsigned char>& data) {
	ctx->streamer->sendAll(resource, data);
}

void WSStreamer::sendAll(string resource, const unsigned char* data, int len) {
	ctx->streamer->sendAll(resource, data, len);
}