#pragma once

#include <string>
#include <vector>

using namespace std;

class WSStreamerCtx;

class WSStreamer {
private:
	WSStreamerCtx* ctx;
public:
	WSStreamer(string host, string port);
	~WSStreamer();
	void operator()();

	// Functions to start and stop WSStreamer
	void run();
	void runAndBlock();
	void stop();

	// Functions to add/remove accepted resources
	void addResource(string rsrc);
	void removeResource(string rsrc);

	// Sends to all clients who requested the specified resource
	void sendAll(string resource, const vector<unsigned char>& data);
	void sendAll(string resource, const unsigned char* data, int len);
};