#include <string>
#include <fstream>
#include <vector>

#include "boost/shared_ptr.hpp"
#include "boost/asio.hpp"
#include "websocketpp.hpp"
#include "websocket_connection_handler.hpp"

#include "utils.h"
#include "WSStreamerCtx.h"

using namespace std;

WSStreamerHandler::WSStreamerHandler() {
	debug.open("wssender.264", ios::binary);
}
WSStreamerHandler::~WSStreamerHandler() {
	debug.close();
}

void WSStreamerHandler::validate(websocketpp::session_ptr client) {
	// Check if requested resource exists
	boost::mutex::scoped_lock lock(connections_mutex);
	if (client->get_resource() == "/") {
		cout << "INFO: Client is connecting without asking for a resource" << endl;
	}
	else if (connections.find(client->get_resource()) != connections.end()) {
		cout << "INFO: Client request for " + client->get_resource() + " accepted" << endl;
	}
	else {
		string err = "Request for unknown resource " + client->get_resource();
		cerr << err << endl;
		throw(websocketpp::handshake_error(err, 404));
	}
}

void WSStreamerHandler::on_open(websocketpp::session_ptr client) {
	boost::mutex::scoped_lock lock(connections_mutex);
	if (client->get_resource() != "/") {
		connections.find(client->get_resource())->second.push_back(client);
		cout << "INFO: Client has connected to " + client->get_resource() << endl;
	}
}

void WSStreamerHandler::on_close(websocketpp::session_ptr client) {
	boost::mutex::scoped_lock lock(connections_mutex);
	map<string, vector<websocketpp::session_ptr>>::iterator client_list = connections.find(client->get_resource());
	if (client_list != connections.end()) {
		vector<websocketpp::session_ptr>::iterator client_itr = find(client_list->second.begin(), client_list->second.end(), client);
		if (client_itr != client_list->second.end()) {
			client_list->second.erase(client_itr);
		}
	}
}

void WSStreamerHandler::on_message(websocketpp::session_ptr client, const std::string &msg) {
	cout << "CLIENTMSG: " << msg << endl;
	// For now, do nothing when clients send messages
}

void WSStreamerHandler::on_message(websocketpp::session_ptr client,
	const std::vector<unsigned char> &data) {
	// Ignore binary data
	cerr << "WARNING: Discarding binary data received from client" << endl;
}

void WSStreamerHandler::addResource(string rsrc) {
	boost::mutex::scoped_lock lock(connections_mutex);
	connections.insert(pair<string, vector<websocketpp::session_ptr>>(rsrc, vector<websocketpp::session_ptr>()));
	cout << "INFO: Resource " << rsrc << " added" << endl;
}

void WSStreamerHandler::removeResource(string rsrc) {
	boost::mutex::scoped_lock lock(connections_mutex);
	map<string, vector<websocketpp::session_ptr>>::iterator client_list = connections.find(rsrc);
	if (client_list != connections.end()) {
		vector<websocketpp::session_ptr>& clients = client_list->second;
		for (size_t i = 0; i < clients.size(); i++) {
			clients[i]->close(websocketpp::session::CLOSE_STATUS_GOING_AWAY, "Resource is no longer available.");
		}
	}
	connections.erase(client_list);
	cout << "INFO: Resource " << rsrc << " removed and all associated clients have been disconnected" << endl;
}

void WSStreamerHandler::sendAll(string resource, const vector<unsigned char>& data) {
	connections_mutex.lock();
	map<string, vector<websocketpp::session_ptr>>::iterator client_list = connections.find(resource);
	vector<websocketpp::session_ptr> clients;
	if (client_list != connections.end()) {
		clients = client_list->second;
	}
	else {
		cout << "WARNING: WSStreamer attempted to send to a non-existent resource" << endl;
	}
	connections_mutex.unlock();

	cout << "INFO: Sending to all clients associated with " << resource << endl;
	debug.write((char*)&data[0], data.size());
	for (size_t i = 0; i < clients.size(); i++) {
		clients[i]->send(data);
	}
}

void WSStreamerHandler::sendAll(string resource, const unsigned char* data, int len) {
	vector<unsigned char> buffer;
	buffer.assign(data, data+len);
	sendAll(resource, buffer);
}


WSStreamerCtx::WSStreamerCtx(string host, string port) : streamer(new WSStreamerHandler()),
	endpoint(tcp::v6(), atoi(port.c_str())),
	server(new websocketpp::server(io_service, endpoint, streamer))
{
	
}