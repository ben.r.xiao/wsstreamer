#include <string>
#include <vector>
#include "boost/thread.hpp"
#include "utils.h"

using namespace std;

void tokenize(const string line, vector<string>& tokens, string delimiter) {
        // Substring positions
        size_t begin = 0;
        size_t end = 0;

        while (end < line.length()) {
                // Extract token into temp
                end = line.find_first_of(delimiter, begin);
                string temp = line.substr(begin, end - begin);

                // Put temp into vector if it isn't an empty string
                if (temp.length() != 0) {
                        tokens.push_back(temp);
                }

                begin = end + 1;
        }
}

// Divides a number in numunit format (e.g. 5KB) into separate num and unit (e.g. 5 KB)
// Splits string at first non-digit in the string line
void splitIntUnit(const string line, int& num, string& unit) {
	size_t split;
	for (split = line.size() - 1; split >= 0 && isalpha(line[split]); split--) {
	}
	num = atoi(line.substr(0, split + 1).c_str());
	unit = line.substr(split + 1);
}

vector<unsigned char> parseNAL(const vector<unsigned char>& bitstream) {
	vector<unsigned char> buffer;
	size_t read_pos;
	// Check if stream starts with a start code. If so skip it because the code is added in the while loop
	if (bitstream.size() >= 4 && bitstream[0] == 0 && bitstream[1] == 0 && bitstream[2] == 0 && bitstream[3] == 1) {
		read_pos = 4;
	}
	else {
		read_pos = 0;
	}

	while (read_pos < bitstream.size()) {
		int end_count = 0;
		// Put in start code of NAL unit
		buffer.push_back(0);
		buffer.push_back(0);
		buffer.push_back(0);
		buffer.push_back(1);
		// Extract bytes and put into vector for sending
		while (read_pos < bitstream.size() && end_count < 3) {
			buffer.push_back(bitstream[read_pos]);
			if (bitstream[read_pos] == 0) {
				end_count++;
			}
			else {
				end_count = 0;
			}
			read_pos++;
		}
		// Increment read pointer by one to skip start code if necessary
		if (read_pos < bitstream.size() - 1 && bitstream[read_pos] == 1) {
			read_pos++;
		}
	}

	// Check if stream ends with a end code. If not, add it in
	if (!(bitstream.size() >= 4 && bitstream[bitstream.size() - 4] == 0 && bitstream[bitstream.size() - 3] == 0 && bitstream[bitstream.size() - 2] == 0 && bitstream[bitstream.size() - 1] == 1)
		&& !(bitstream.size() >= 3 && bitstream[bitstream.size() - 3] == 0 && bitstream[bitstream.size() - 2] == 0 && bitstream[bitstream.size() - 1] == 0)) {
		buffer.push_back(0);
		buffer.push_back(0);
		buffer.push_back(0);
	}
	return buffer;
}

vector<unsigned char> parseNAL(unsigned char* data, int len) {
	vector<unsigned char> stream(data, data + len);
	return parseNAL(stream);
}